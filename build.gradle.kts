/*
 * Copyright © 2021. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

group = "com.jaspery.jaspery-public-libraries"
version = "1.0.0"

plugins {
    `java-platform`
    `maven-publish`
}

publishing {
    publications {
        create<MavenPublication>("jplDependenciesPlatform") {
            if (project.hasProperty("publishToMavenRepo")) {
                val buildBranch: String? by project
                val buildNumber: String by project
                val defaultBranch: String by project

                val versionBranchPart = buildBranch?.takeUnless { it == defaultBranch }?.let { "-$it" } ?: ""
                val versionBuildNumberPart = buildNumber.takeUnless { it.isEmpty() }
                        ?.let { "-${it.padStart(6, '0')}" } ?: ""

                version = "${project.version}$versionBuildNumberPart$versionBranchPart"
            }

            from(components["javaPlatform"])
        }
    }

    repositories {
        if (project.hasProperty("publishToMavenRepo")) {
            val publishMavenRepoUrl: String by project
            val credentialsTokenType: String by project
            val credentialsTokenValue: String by project

            maven(publishMavenRepoUrl) {
                name = "GitLab-${project.name}"
                credentials(HttpHeaderCredentials::class) {
                    name = credentialsTokenType
                    value = credentialsTokenValue
                }
                authentication {
                    create<HttpHeaderAuthentication>("header")
                }
            }
        }
    }
}

dependencies {
    @Suppress("UnstableApiUsage")
    constraints {
        val kotlinVersion: String by project
        val assertkVersion = "0.23.1"
        val koinVersion = "3.0.1-alpha-6"
        val mockkVersion = "1.10.3"
        val testngVersion = "7.4.0"
        val jplJKotlinExt = "-SNAPSHOT"

        api(kotlin("stdlib-jdk8", kotlinVersion))
        api(kotlin("reflect", kotlinVersion))
        api(kotlin("gradle-plugin")) {
            version {
                require(embeddedKotlinVersion)
                prefer(kotlinVersion)
            }
        }

        api("com.willowtreeapps.assertk:assertk-jvm:$assertkVersion")
        api("com.willowtreeapps.assertk:assertk-metadata:$assertkVersion")

        api("io.insert-koin:koin-core:$koinVersion")

        api("io.insert-koin:koin-test:$koinVersion")
        api("io.mockk:mockk:$mockkVersion")
        api(jpl("jaspery-kotlin-ext:kotlin-testng-dataprovider", jplJKotlinExt))
        // Tame transitive dependencies
        api("org.testng:testng:$testngVersion")
    }
}

@Suppress("unused")
fun DependencyConstraintHandlerScope.jpl(simpleModuleName: String, version: String? = null): String =
        "com.gitlab.jaspery-public-libraries.$simpleModuleName" + version?.let { ":$it" }.orEmpty()
